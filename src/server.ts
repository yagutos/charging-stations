import app from './config/app';
import env from './environment';
import { Logger } from './util/Logger';

const PORT = env.getPort();

app.listen(PORT, () => {
  Logger.info('Express server listening on port ' + PORT);
});
