import { Request, Response } from 'express';
import {
  insufficientParameters,
  mongoError,
  successResponse
} from '../util/apiResponse';
import { StationService } from '../services/StationService';
import { ValidationError, Result, validationResult } from 'express-validator';
import { IStation } from 'types/station';
import { CompanyService } from '../services/CompanyService';

export class StationController {
  private stationService: StationService = new StationService();
  private companyService: CompanyService = new CompanyService();

  public async createStation(req: Request, res: Response): Promise<void> {
    try {
      const errors: Result<ValidationError> = validationResult(req);
      if (!errors.isEmpty()) {
        return insufficientParameters(res, errors.array());
      }

      const savedStation: IStation = await this.stationService.createStation(
        req.body
      );

      return successResponse('station_is_created', savedStation, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getStation(req: Request, res: Response): Promise<void> {
    try {
      const station: IStation = await this.stationService.getStation(
        req.params.id
      );
      return successResponse('station', station, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getStations(req: Request, res: Response): Promise<void> {
    try {
      const stations: IStation[] = await this.stationService.getStations();
      return successResponse('stations', stations, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getStationsWithCompanies(
    req: Request,
    res: Response
  ): Promise<void> {
    try {
      const stations: IStation[] = await this.stationService.getStationsWithCompanies();
      return successResponse('stations', stations, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getStationsByCompanyId(
    req: Request,
    res: Response
  ): Promise<void> {
    try {
      const parentCompanyId = req.params.id;
      const childCompanyIds = await this.companyService.getCompanyChildrenIds(
        parentCompanyId
      );

      const stations: IStation[] = await this.stationService.getStationsByCompanyIds(
        [parentCompanyId, ...childCompanyIds]
      );
      return successResponse('stations', stations, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getNearStations(req: Request, res: Response): Promise<void> {
    try {
      const stations: IStation[] = await this.stationService.getNearStations(
        parseFloat(req.params.longitude),
        parseFloat(req.params.latitude)
      );
      return successResponse('stations', stations, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async updateStation(req: Request, res: Response): Promise<void> {
    try {
      const station: IStation = await this.stationService.updateStation(
        req.params.id,
        req.body
      );
      return successResponse('update station', station, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async deleteStation(req: Request, res: Response): Promise<void> {
    try {
      await this.stationService.deleteStation(req.params.id);
      return successResponse('delete station', null, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }
}
