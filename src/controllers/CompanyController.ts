import { Request, Response } from 'express';
import {
  insufficientParameters,
  mongoError,
  successResponse
} from '../util/apiResponse';
import { CompanyService } from '../services/CompanyService';
import { ICompany } from 'types/company';
import { ValidationError, Result, validationResult } from 'express-validator';

export class CompanyController {
  private companyService: CompanyService = new CompanyService();

  public async createCompany(req: Request, res: Response): Promise<void> {
    try {
      const errors: Result<ValidationError> = validationResult(req);
      if (!errors.isEmpty()) {
        return insufficientParameters(res, errors.array());
      }

      const savedCompany: ICompany = await this.companyService.createCompany(
        req.body
      );

      return successResponse('company_is_created', savedCompany, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getCompany(req: Request, res: Response): Promise<void> {
    try {
      const company: ICompany = await this.companyService.getCompany(
        req.params.id
      );
      return successResponse('company', company, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getCompanyHierarchy(req: Request, res: Response): Promise<void> {
    try {
      const companies: ICompany[] = await this.companyService.getCompanyHierarchy(
        req.params.id
      );
      return successResponse('Company Full Hierarchy', companies, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getCompanies(req: Request, res: Response): Promise<void> {
    try {
      const companies: ICompany[] = await this.companyService.getCompanies();
      return successResponse('companies', companies, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getCompaniesByParent(
    req: Request,
    res: Response
  ): Promise<void> {
    try {
      const companies: ICompany[] = await this.companyService.getCompaniesByParent(
        req.params.id !== 'null' ? req.params.id : null
      );
      return successResponse('companies', companies, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async getCompaniesByName(req: Request, res: Response): Promise<void> {
    try {
      const companies: ICompany[] = await this.companyService.getCompaniesByName(
        req.params.query
      );
      return successResponse('companies', companies, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async updateCompany(req: Request, res: Response): Promise<void> {
    try {
      const company: ICompany = await this.companyService.updateCompany(
        req.params.id,
        req.body
      );
      return successResponse('update company', company, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }

  public async deleteCompany(req: Request, res: Response): Promise<void> {
    try {
      await this.companyService.deleteCompany(req.params.id);
      return successResponse('delete company', null, res);
    } catch (error) {
      return mongoError(error, res);
    }
  }
}
