import { model, Schema } from 'mongoose';
import { ICompany } from 'types/company';

const schema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    parent_company: {
      type: Schema.Types.ObjectId,
      ref: 'companies'
    },
    hasChildren: {
      type: Boolean,
      default: false
    },
    ancestors: [
      {
        type: Schema.Types.ObjectId,
        ref: 'companies'
      }
    ]
  },
  { timestamps: true }
);
schema.index({ ancestors: 1 });

export default model<ICompany>('companies', schema);
