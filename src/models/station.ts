import { model, Schema } from 'mongoose';
import { IStation } from 'types/station';

const schema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    type: {
      type: String,
      enum: ['Point'],
      default: 'Point'
    },
    coordinates: [Number],
    company: {
      type: Schema.Types.ObjectId,
      ref: 'companies'
    }
  },
  { timestamps: true }
);

schema.index({ coordinates: '2dsphere' });

export default model<IStation>('stations', schema);
