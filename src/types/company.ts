import { Document } from 'mongoose';

export interface ICompany extends Document {
  name: string;
  parent_company?: string;
  ancestors: string[];
  hasChildren: boolean;
}
