export interface ApiResponse {
  status: string;
  message: string;
  data: unknown;
  errors?: unknown[];
}
