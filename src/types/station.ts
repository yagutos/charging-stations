import { Document } from 'mongoose';
import { ICompany } from './company';

export interface IStation extends Document {
  name: string;
  coordinates?: number[];
  company: string | ICompany;
}
