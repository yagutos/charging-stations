import { Router } from 'express';
import { check } from 'express-validator';
import { CompanyController } from '../controllers/CompanyController';

const router = Router();
const companyController: CompanyController = new CompanyController();

router.post(
  '/',
  [check('name', 'Please add a name').not().isEmpty()],
  companyController.createCompany.bind(companyController)
);

router.put(
  '/:id',
  [check('name', 'Please add a name').not().isEmpty()],
  companyController.updateCompany.bind(companyController)
);

router.get(
  '/hierarchy/:id',
  companyController.getCompanyHierarchy.bind(companyController)
);
router.get(
  '/byParent/:id',
  companyController.getCompaniesByParent.bind(companyController)
);
router.get(
  '/query/:query',
  companyController.getCompaniesByName.bind(companyController)
);
router.get('/:id', companyController.getCompany.bind(companyController));

router.get('/', companyController.getCompanies.bind(companyController));

router.delete('/:id', companyController.deleteCompany.bind(companyController));

export default router;
