import { Router } from 'express';
import { check } from 'express-validator';
import { StationController } from '../controllers/StationController';

const router = Router();
const stationController: StationController = new StationController();

router.post(
  '/',
  [
    check('name', 'Please add a name').not().isEmpty(),
    check('coordinates', 'Please add a coordinates').not().isEmpty(),
    check('company', 'Missing company').not().isEmpty()
  ],
  stationController.createStation.bind(stationController)
);

router.put(
  '/:id',
  [
    check('name', 'Please add a name').not().isEmpty(),
    check('coordinates', 'Please add a coordinates').not().isEmpty(),
    check('company', 'Missing company').not().isEmpty()
  ],
  stationController.updateStation.bind(stationController)
);

router.get(
  '/with-companies',
  stationController.getStationsWithCompanies.bind(stationController)
);
router.get(
  '/byCompanyId/:id',
  stationController.getStationsByCompanyId.bind(stationController)
);
router.get(
  '/nearest/:longitude/:latitude',
  stationController.getNearStations.bind(stationController)
);
router.get('/:id', stationController.getStation.bind(stationController));
router.get('/', stationController.getStations.bind(stationController));

router.delete('/:id', stationController.deleteStation.bind(stationController));

export default router;
