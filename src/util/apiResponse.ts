import { Response } from 'express';
import { ValidationError } from 'express-validator';
import { ApiResponse } from '../types/ApiResponse';
import env from '../environment';

export enum ResponseStatusCodes {
  Success = 200,
  BadRequest = 400,
  InternalServerError = 500
}

export function successResponse(
  message: string,
  data: unknown,
  res: Response
): void {
  res.status(ResponseStatusCodes.Success).json(<ApiResponse>{
    status: 'SUCCESS',
    message,
    data
  });
}

export function failureResponse(
  message: string,
  data: unknown,
  res: Response
): void {
  res.status(ResponseStatusCodes.Success).json(<ApiResponse>{
    status: 'FAILURE',
    message,
    data
  });
}

export function insufficientParameters(
  res: Response,
  errors?: ValidationError[]
): void {
  res.status(ResponseStatusCodes.BadRequest).json(<ApiResponse>{
    status: 'FAILURE',
    message: 'Insufficient parameters',
    data: {},
    errors: errors
  });
}

export function mongoError(err: Error, res: Response): void {
  res.status(ResponseStatusCodes.InternalServerError).json(<ApiResponse>{
    status: 'FAILURE',
    message: 'MongoDB error',
    errors: !env.isProductionMode() ? [err.stack] : [err.message]
  });
}
