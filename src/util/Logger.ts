import * as winston from 'winston';

export class Logger {
  private static myLogger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [new winston.transports.Console()]
  });

  static error(msg: unknown): void {
    this.myLogger.error({
      timeStamp: new Date().toLocaleString(),
      message: msg
    });
  }

  static info(msg: unknown): void {
    this.myLogger.info({
      timeStamp: new Date().toLocaleString(),
      message: msg
    });
  }
}
