import Companies from '../models/company';
import { ICompany } from 'types/company';
import { StationService } from './StationService';

export class CompanyService {
  private stationService: StationService = new StationService();

  public async createCompany(company: ICompany): Promise<ICompany> {
    await this.setCompanyAncestors(company);
    await this.setCompanyHasChildren(company.parent_company, true);

    const _session = new Companies(company);
    const savedCompany: ICompany = await _session.save();

    return savedCompany;
  }

  public async getCompany(id: string): Promise<ICompany> {
    const company: ICompany = await Companies.findOne({ _id: id });

    return company;
  }

  public async getCompanyHierarchy(id: string): Promise<ICompany[]> {
    const company: ICompany = await this.getCompany(id);
    const childCompanies: ICompany[] = await Companies.find({
      ancestors: {
        $in: [id]
      }
    });

    return [company, ...childCompanies];
  }

  public async getCompanies(): Promise<ICompany[]> {
    const companies: ICompany[] = await Companies.find({
      parent_company: null
    });
    return companies;
  }

  public async getCompaniesByName(query: string): Promise<ICompany[]> {
    if (!query) return [];
    const companies: ICompany[] = await Companies.find({
      name: {
        $regex: query,
        $options: 'i'
      }
    });
    return companies;
  }

  public async getCompaniesByParent(id: string | null): Promise<ICompany[]> {
    const companies: ICompany[] = await Companies.find({
      parent_company: id
    });
    return companies;
  }

  public async updateCompany(
    id: string,
    company: Partial<ICompany>
  ): Promise<ICompany> {
    const updatedCompany: ICompany = await Companies.findOneAndUpdate(
      { _id: id },
      { $set: company },
      { new: true }
    );
    return updatedCompany;
  }

  public async deleteCompany(id: string): Promise<void> {
    const company = await this.getCompany(id);
    await this.deleteCompanyChildren(id);
    await this.stationService.deleteStationsByCompanyIds([
      id,
      ...(await this.getCompanyChildrenIds(id))
    ]);
    await Companies.findByIdAndRemove(id);
    await this.checkParentChildren(company.parent_company);
  }

  private async deleteCompanyChildren(id: string): Promise<void> {
    await Companies.deleteMany({
      ancestors: {
        $in: [id]
      }
    });
  }

  private async getCompanyAncestors(id: string): Promise<string[]> {
    return await (await Companies.findOne({ _id: id })).ancestors;
  }

  private async getCompanyChildren(id: string): Promise<ICompany[]> {
    return await Companies.find({ ancestors: { $in: [id] } });
  }

  public async getCompanyChildrenIds(id: string): Promise<string[]> {
    const companies = await this.getCompanyChildren(id);
    return companies.map((company) => company._id);
  }

  private async setCompanyAncestors(company: ICompany) {
    if (!company.parent_company) {
      return;
    }

    const ancestorpath: string[] = await this.getCompanyAncestors(
      company.parent_company
    );
    ancestorpath.push(company.parent_company);
    company.ancestors = ancestorpath;
  }

  private async setCompanyHasChildren(
    id: string,
    hasChildren: boolean
  ): Promise<void> {
    if (!id) {
      return;
    }

    this.updateCompany(id, {
      hasChildren: hasChildren
    });
  }

  private async checkParentChildren(id: string): Promise<void> {
    const children = await this.getCompaniesByParent(id);
    if (children.length === 0) {
      this.setCompanyHasChildren(id, false);
    }
  }
}
