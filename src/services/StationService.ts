import Stations from '../models/station';
import { IStation } from 'types/station';

export class StationService {
  public async createStation(station: IStation): Promise<IStation> {
    const _session = new Stations(station);
    const savedStation: IStation = await _session.save();

    return savedStation;
  }

  public async getStation(id: string): Promise<IStation> {
    const station: IStation = await Stations.findOne({ _id: id });
    return station;
  }

  public async getStations(): Promise<IStation[]> {
    const stations: IStation[] = await Stations.find({});
    return stations;
  }

  public async getNearStations(
    longitude: number,
    latitude: number
  ): Promise<IStation[]> {
    // const stations: IStation[] = await Stations.aggregate([
    //   {
    //     $geoNear: {
    //       near: {
    //         type: 'Point',
    //         coordinates: [longitude, latitude]
    //       },
    //       distanceField: 'distance',
    //       spherical: true
    //     }
    //   }
    // ]).limit(50);

    const stations: IStation[] = await Stations.aggregate([
      {
        $geoNear: {
          near: {
            type: 'Point',
            coordinates: [longitude, latitude]
          },
          distanceField: 'distance',
          spherical: true
        }
      },
      {
        $lookup: {
          from: 'companies',
          localField: 'company',
          foreignField: '_id',
          as: 'company'
        }
      },
      { $unwind: '$company' }
    ]).limit(50);

    return stations;
  }

  public async getStationsByCompanyIds(ids: string[]): Promise<IStation[]> {
    const stations: IStation[] = await Stations.find({
      company: { $in: ids }
    }).populate('company');
    return stations;
  }

  public async getStationsWithCompanies(): Promise<IStation[]> {
    const stations: IStation[] = await Stations.find({}).populate('company');
    return stations;
  }

  public async updateStation(id: string, station: IStation): Promise<IStation> {
    const updatedStation: IStation = await Stations.findOneAndUpdate(
      {
        _id: id
      },
      {
        $set: station
      },
      {
        new: true
      }
    );
    return updatedStation;
  }

  public async deleteStation(id: string): Promise<void> {
    await Stations.findByIdAndRemove(id);
  }

  public async deleteStationsByCompanyIds(ids: string[]): Promise<void> {
    await Stations.deleteMany({
      company: {
        $in: ids
      }
    });
  }
}
