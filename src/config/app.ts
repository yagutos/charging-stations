import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import { connect } from 'mongoose';
import { CommonRoutes } from '../routes/commonRoutes';

import env from '../environment';
import companyRoutes from '../routes/companyRoutes';
import stationRoutes from '../routes/stationRoutes';

class App {
  public app: express.Application;

  public mongoUrl: string = env.getDBUrl();

  //route initializing
  private commonRoutes: CommonRoutes = new CommonRoutes();

  constructor() {
    this.app = express();
    this.config();

    this.mongoSetup();

    //build routes
    this.app.use('/api/companies', companyRoutes);
    this.app.use('/api/stations', stationRoutes);
    this.redirectToAngular();
    this.commonRoutes.route(this.app);
  }

  private redirectToAngular() {
    // Serve static assets in production
    if (env.isProductionMode()) {
      this.app.use(express.static('client/dist/charging-stations-ui'));
      this.app.get('*', (req, res) =>
        res.sendFile(
          path.resolve(
            __dirname,
            '../../client',
            'dist',
            'charging-stations-ui',
            'index.html'
          )
        )
      );
    }
  }

  private config(): void {
    // support application/json type post data
    this.app.use(bodyParser.json());
    //support application/x-www-form-urlencoded post data
    this.app.use(bodyParser.urlencoded({ extended: false }));

    if (!env.isProductionMode()) {
      this.app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4000');
        res.setHeader(
          'Access-Control-Allow-Headers',
          'Accept,Accept-Language,Content-Language,Content-Type'
        );

        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        next();
      });
    }
  }

  private mongoSetup(): void {
    connect(this.mongoUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    });
  }
}

export default new App().app;
