class Environment {
  getPort(): number {
    if (process.env.PORT) {
      return parseInt(process.env.PORT);
    } else {
      return 3000;
    }
  }

  getDBName(): string {
    if (this.isProductionMode()) {
      return 'db_test_project_prod';
    } else {
      return 'db_test_project_dev';
    }
  }

  getDBUrl(): string {
    if (this.isProductionMode()) {
      return 'mongodb+srv://brad123:TUjs4dCThMJJyoWR@contactkeeper.mkiwa.mongodb.net/<dbname>?retryWrites=true&w=majority';
    } else {
      return 'mongodb+srv://brad123:TUjs4dCThMJJyoWR@contactkeeper.mkiwa.mongodb.net/<dbname>?retryWrites=true&w=majority';
    }
  }

  isProductionMode(): boolean {
    return process.env.NODE_ENV === 'production';
  }
}

export default new Environment();
