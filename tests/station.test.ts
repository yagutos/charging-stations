//import { Options } from '../src/Classes/Options/Options'; // this will be your custom import
import * as chai from 'chai';
import chaiHttp = require('chai-http');
import app from '../src/config/app';

chai.use(chaiHttp);
const expect = chai.expect;

const testCompanyId = '5f5e62e37c87d35048df1696';
let stationId;

describe('test station simple scenarios', () => {
  // the tests container
  describe('test station CRUD scenarios', () => {
    // the tests container
    it('required params are missing', (done) => {
      // the single test
      chai
        .request(app)
        .post('/api/stations')
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it('all params are filled for add', (done) => {
      // the single test
      chai
        .request(app)
        .post('/api/stations')
        .send({
          name: 'Test station 1',
          company: testCompanyId,
          coordinates: [44.7752845, 41.725722]
        })
        .end((err, res) => {
          stationId = res.body.data._id;
          expect(res).to.have.status(200);
          done();
        });
    });
    it('should update station data', (done) => {
      // the single test
      chai
        .request(app)
        .put(`/api/stations/${stationId}`)
        .send({
          name: 'Test station 2 Update'
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get station', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/stations/${stationId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get station by company id', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/stations/byCompanyId/${testCompanyId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get nearest stations', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/stations/nearest/44.7752845/41.725722`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get station', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/stations`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should delete station', (done) => {
      // the single test
      chai
        .request(app)
        .delete(`/api/stations/${stationId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
