//import { Options } from '../src/Classes/Options/Options'; // this will be your custom import
import * as chai from 'chai';
import chaiHttp = require('chai-http');
import app from '../src/config/app';

chai.use(chaiHttp);
const expect = chai.expect;

let companyId;

describe('test company simple scenarios', () => {
  // the tests container
  describe('test company CRUD scenarios', () => {
    // the tests container
    it('required params are missing', (done) => {
      // the single test
      chai
        .request(app)
        .post('/api/companies')
        .end((err, res) => {
          expect(res).to.have.status(400);
          done();
        });
    });

    it('all params are filled for add', (done) => {
      // the single test
      chai
        .request(app)
        .post('/api/companies')
        .send({
          name: 'Test Company'
        })
        .end((err, res) => {
          companyId = res.body.data._id;
          expect(res).to.have.status(200);
          done();
        });
    });
    it('should update company data', (done) => {
      // the single test
      chai
        .request(app)
        .put(`/api/companies/${companyId}`)
        .send({
          name: 'Test Company Update'
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get company', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/companies/${companyId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get company hierarchy', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/companies/hierarchy/${companyId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get company by name', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/companies/query/test`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should get companies', (done) => {
      // the single test
      chai
        .request(app)
        .get(`/api/companies`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });

    it('should delete company', (done) => {
      // the single test
      chai
        .request(app)
        .delete(`/api/companies/${companyId}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
