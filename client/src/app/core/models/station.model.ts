import { Company } from './company.model';

export interface Station {
  _id?: string;
  name: string;
  coordinates: number[];
  company: string | Company;
}
