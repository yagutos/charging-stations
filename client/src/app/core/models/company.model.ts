export interface Company {
  _id?: string;
  name: string;
  parent_company?: string;
  hasChildren?: true;
  ancestors?: string[];
}
