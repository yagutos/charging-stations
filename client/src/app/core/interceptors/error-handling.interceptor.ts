import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { MessageService } from 'primeng/api';
import { NotificationsService } from 'src/app/shared/components/notifications/notifications.service';

/**
 * Adds a default error handler to all requests.
 */
@Injectable()
export class ErrorHandlingInterceptor implements HttpInterceptor {
  constructor(private notificationsService: NotificationsService) {}
  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next
      .handle(request)
      .pipe(catchError((error) => this.errorHandler(error)));
  }

  private errorHandler(
    response: HttpErrorResponse
  ): Observable<HttpEvent<HttpErrorResponse>> {
    const errorMessage = response.error
      ? response.error.message
      : response.message;
    const detail = response.error ? response.error.errors : null;

    this.notificationsService.notify('error', errorMessage, detail);

    throw response;
  }
}
