import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { Station } from '../../models/station.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Response } from '../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class StationService {
  constructor(private apiService: ApiService) {}

  getStations(): Observable<Station[]> {
    return this.apiService
      .get<Response<Station[]>>('/api/stations')
      .pipe(map((res) => res.data));
  }

  getStationsWithCompanyData(): Observable<Station[]> {
    return this.apiService
      .get<Response<Station[]>>('/api/stations/with-companies')
      .pipe(map((res) => res.data));
  }

  getStationsByCompanyId(id: string): Observable<Station[]> {
    return this.apiService
      .get<Response<Station[]>>(`/api/stations/byCompanyId/${id}`)
      .pipe(map((res) => res.data));
  }

  getNearestStations(
    latitude: number,
    longitude: number
  ): Observable<Station[]> {
    return this.apiService
      .get<Response<Station[]>>(
        `/api/stations/nearest/${longitude}/${latitude}`
      )
      .pipe(map((res) => res.data));
  }

  getStation(id: string): Observable<Station> {
    return this.apiService
      .get<Response<Station>>(`/api/stations/${id}`)
      .pipe(map((res) => res.data));
  }

  addStation(station: Station): Observable<Station> {
    return this.apiService
      .post<Response<Station>>('/api/stations', station)
      .pipe(map((res) => res.data));
  }

  updateStation(id: string, station: Station): Observable<Station> {
    return this.apiService
      .put<Response<Station>>(`/api/stations/${id}`, station)
      .pipe(map((res) => res.data));
  }

  deleteStation(id: string): Observable<Station> {
    return this.apiService
      .delete<Response<Station>>(`/api/stations/${id}`)
      .pipe(map((res) => res.data));
  }
}
