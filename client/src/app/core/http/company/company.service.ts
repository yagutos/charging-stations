import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Company } from '../../models/company.model';
import { Response } from '../../models/response.model';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  constructor(private apiService: ApiService) {}

  getCompanies(): Observable<Company[]> {
    return this.apiService
      .get<Response<Company[]>>('/api/companies')
      .pipe(map((res) => res.data));
  }

  getCompany(id: string): Observable<Company> {
    return this.apiService
      .get<Response<Company>>(`/api/companies/${id}`)
      .pipe(map((res) => res.data));
  }
  getCompanyHierarchy(id: string): Observable<Company> {
    return this.apiService
      .get<Response<Company>>(`/api/companies/hierarchy/${id}`)
      .pipe(map((res) => res.data));
  }

  getCompaniesByName(query: string): Observable<Company[]> {
    return this.apiService
      .get<Response<Company[]>>(`/api/companies/query/${query}`)
      .pipe(map((res) => res.data));
  }

  getCompaniesByParent(id: string | null): Observable<Company[]> {
    return this.apiService
      .get<Response<Company[]>>(`/api/companies/byParent/${id}`)
      .pipe(map((res) => res.data));
  }

  addCompany(company: Company): Observable<Company> {
    return this.apiService
      .post<Response<Company>>('/api/companies', company)
      .pipe(map((res) => res.data));
  }

  updateCompany(id: string, company: Company): Observable<Company> {
    return this.apiService
      .put<Response<Company>>(`/api/companies/${id}`, company)
      .pipe(map((res) => res.data));
  }

  deleteCompany(id: string): Observable<Company> {
    return this.apiService
      .delete<Response<Company>>(`/api/companies/${id}`)
      .pipe(map((res) => res.data));
  }
}
