import { createReducer, on } from '@ngrx/store';
import { EntityState, createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { companyActions } from './company.actions';
import { Company } from 'src/app/core/models/company.model';

export interface ICompanyState extends EntityState<Company> {
  companies: Company[];
  selectedCompany: Company;
  loading: boolean;
}

export const adapter: EntityAdapter<Company> = createEntityAdapter<Company>({
  selectId: (company: Company) => company._id
});

export const initialCompanyState = adapter.getInitialState({
  companies: null,
  selectedCompany: null,
  loading: true
});

export const companyReducer = createReducer(
  initialCompanyState,

  on(companyActions.companiesLoaded, (state, action) => {
    return adapter.addAll(action.companies, { ...state, loading: false });
  }),

  on(companyActions.createCompanySuccess, (state, action) => {
    return adapter.addOne(action.company, state);
  }),

  on(companyActions.deleteCompany, (state, action) => {
    return adapter.removeOne(action.id, state);
  }),

  on(companyActions.updateCompany, (state, action) => {
    return adapter.updateOne(action.update, state);
  })
);

export const { selectAll, selectIds } = adapter.getSelectors();
