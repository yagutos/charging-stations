import { companyActions } from './company.actions';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { concatMap, map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CompanyService } from 'src/app/core/http/company/company.service';
import { Company } from 'src/app/core/models/company.model';

@Injectable()
export class CompanyEffects {
  loadCompanies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(companyActions.loadCompanies),
      concatMap(() => this.companyService.getCompanies()),
      map((companies) => companyActions.companiesLoaded({ companies }))
    )
  );

  createCompany$ = createEffect(() =>
    this.actions$.pipe(
      ofType(companyActions.createCompany),
      concatMap((action) => this.companyService.addCompany(action.company)),
      map((company) => companyActions.createCompanySuccess({ company }))
    )
  );

  deleteCompany$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(companyActions.deleteCompany),
        concatMap((action) => this.companyService.deleteCompany(action.id))
      ),
    { dispatch: false }
  );

  updateCompany$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(companyActions.updateCompany),
        concatMap((action) =>
          this.companyService.updateCompany(
            action.update.id as string,
            action.update.changes as Company
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private companyService: CompanyService,
    private actions$: Actions
  ) {}
}
