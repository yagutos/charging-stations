import { ICompanyState, selectAll } from './company.reducer';
import { createSelector, createFeatureSelector } from '@ngrx/store';

export const companyFeatureSelector = createFeatureSelector<ICompanyState>(
  'companies'
);

export const getAllCompanies = createSelector(
  companyFeatureSelector,
  selectAll
);

export const getSelected = createSelector(
  companyFeatureSelector,
  (state) => state.selectedCompany
);

export const areCompaniesLoading = createSelector(
  companyFeatureSelector,
  (state) => state.loading
);
