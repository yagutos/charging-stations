import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { Company } from 'src/app/core/models/company.model';

const loadCompanies = createAction('[Companies] Load Companies');

const companiesLoaded = createAction(
  '[Companies] Companies Loaded Successfully',
  props<{ companies: Company[] }>()
);

const createCompany = createAction(
  '[Companies] Create Company',
  props<{ company: Company }>()
);

const createCompanySuccess = createAction(
  '[Companies] Create Company Success',
  props<{ company: Company }>()
);

const deleteCompany = createAction(
  '[Companies] Delete Company',
  props<{ id: string }>()
);

const updateCompany = createAction(
  '[Companies] Update Company',
  props<{ update: Update<Company> }>()
);

export const companyActions = {
  loadCompanies,
  companiesLoaded,
  createCompany,
  createCompanySuccess,
  deleteCompany,
  updateCompany
};
