import { ActionReducerMap } from '@ngrx/store';
import {
  IStationState,
  initialStationState,
  stationReducer
} from './station/station.reducer';
import {
  ICompanyState,
  initialCompanyState,
  companyReducer
} from './company/company.reducer';

export interface IAppState {
  stations: IStationState;
  companies: ICompanyState;
}

export const initialAppState: IAppState = {
  stations: initialStationState,
  companies: initialCompanyState
};

export const appReducers: ActionReducerMap<IAppState> = {
  stations: stationReducer,
  companies: companyReducer
};
