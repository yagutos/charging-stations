import { createReducer, on } from '@ngrx/store';
import { EntityState } from '@ngrx/entity';
import { stationActions } from './station.actions';
import { Station } from 'src/app/core/models/station.model';

export interface IStationState {
  stations: Station[];
  selectedStation: Station;
  loading: boolean;
}

export const initialStationState = {
  stations: null,
  selectedStation: null,
  loading: true
};

export const stationReducer = createReducer(
  initialStationState,

  on(stationActions.stationsLoaded, (state, action) => {
    return { ...state, stations: action.stations, loading: false };
  }),

  on(stationActions.createStationSuccess, (state, action) => {
    return { ...state, stations: [...state.stations, action.station] };
  }),

  on(stationActions.deleteStation, (state, action) => {
    return {
      ...state,
      stations: state.stations.filter((station) => station._id !== action.id)
    };
  }),

  on(stationActions.updateStation, (state, action) => {
    return {
      ...state,
      stations: state.stations.map((station) =>
        station._id === action.update.id ? action.update.changes : station
      )
    };
  }),

  on(stationActions.setLoadingTrue, (state) => {
    return { ...state, loading: true };
  })
);
