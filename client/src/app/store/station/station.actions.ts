import { createAction, props } from '@ngrx/store';
import { Station } from 'src/app/core/models/station.model';
import { Update } from '@ngrx/entity';

const loadStations = createAction('[Stations] Load Stations');
const loadStationsByCompanyId = createAction(
  '[Stations] Load Stations By CompanyId',
  props<{ id: string }>()
);
const loadNearestStations = createAction(
  '[Stations] Load Nearest Stations',
  props<{ latitude: number; longitude: number }>()
);

const stationsLoaded = createAction(
  '[Stations] Stations Loaded Successfully',
  props<{ stations: Station[] }>()
);

const createStation = createAction(
  '[Stations] Create Station',
  props<{ station: Station }>()
);

const createStationSuccess = createAction(
  '[Stations] Create Station Success',
  props<{ station: Station }>()
);

const deleteStation = createAction(
  '[Stations] Delete Station',
  props<{ id: string }>()
);

const updateStation = createAction(
  '[Stations] Update Station',
  props<{ update: Update<Station> }>()
);

const setLoadingTrue = createAction('[Stations] Loading True');
const setLoadingFalse = createAction('[Stations] Loading False');

export const stationActions = {
  loadStations,
  loadStationsByCompanyId,
  loadNearestStations,
  stationsLoaded,
  createStation,
  createStationSuccess,
  deleteStation,
  updateStation,
  setLoadingTrue,
  setLoadingFalse
};
