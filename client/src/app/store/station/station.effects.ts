import { stationActions } from './station.actions';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { concatMap, map, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StationService } from 'src/app/core/http/station/station.service';
import { Station } from 'src/app/core/models/station.model';

@Injectable()
export class StationEffects {
  loadStations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(stationActions.loadStations),
      concatMap(() => this.stationService.getStationsWithCompanyData()),
      map((stations) => stationActions.stationsLoaded({ stations }))
    )
  );

  loadStationsByCompanyId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(stationActions.loadStationsByCompanyId),
      concatMap((action) =>
        this.stationService.getStationsByCompanyId(action.id)
      ),
      map((stations) => stationActions.stationsLoaded({ stations }))
    )
  );

  loadNearestStations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(stationActions.loadNearestStations),
      concatMap((action) =>
        this.stationService.getNearestStations(
          action.latitude,
          action.longitude
        )
      ),
      map((stations) => stationActions.stationsLoaded({ stations }))
    )
  );

  createStation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(stationActions.createStation),
      concatMap((action) => this.stationService.addStation(action.station)),
      map(() => stationActions.loadStations())
    )
  );

  deleteStation$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(stationActions.deleteStation),
        concatMap((action) => this.stationService.deleteStation(action.id))
      ),
    { dispatch: false }
  );

  updateStation$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(stationActions.updateStation),
        concatMap((action) =>
          this.stationService.updateStation(
            action.update.id as string,
            action.update.changes as Station
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private stationService: StationService,
    private actions$: Actions,
    private router: Router
  ) {}
}
