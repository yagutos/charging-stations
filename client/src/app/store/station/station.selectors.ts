import { IStationState } from './station.reducer';
import { createSelector, createFeatureSelector } from '@ngrx/store';

export const stationFeatureSelector = createFeatureSelector<IStationState>(
  'stations'
);

export const getAllStations = createSelector(
  stationFeatureSelector,
  (state) => state.stations
);

export const getSelected = createSelector(
  stationFeatureSelector,
  (state) => state.selectedStation
);

export const areStationsLoading = createSelector(
  stationFeatureSelector,
  (state) => state.loading
);
