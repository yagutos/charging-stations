import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { TableModule } from 'primeng/table';
import { AgmCoreModule } from '@agm/core';
import { ProgressBarModule } from 'primeng/progressbar';
import { SharedModule } from 'src/app/shared/shared.module';
import { DialogModule } from 'primeng/dialog';
import { StationsModule } from '../stations/stations.module';
import { ButtonModule } from 'primeng/button';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    TableModule,
    AgmCoreModule,
    ProgressBarModule,
    SharedModule,
    DialogModule,
    StationsModule,
    ButtonModule,
    TooltipModule
  ]
})
export class HomeModule {}
