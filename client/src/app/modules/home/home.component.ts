import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Station } from 'src/app/core/models/station.model';
import { Coordinates } from 'src/app/core/models/coordinates.model';
import { MapService } from 'src/app/shared/services/map.service';
import { Company } from 'src/app/core/models/company.model';
import { StationService } from 'src/app/core/http/station/station.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  stations$: Observable<Station[]>;

  mapZoom = 12;
  locationDisabled: boolean;
  userCoordinates: Coordinates;

  showDialog = false;
  company: Company;

  constructor(
    private mapService: MapService,
    private stationService: StationService
  ) {}

  ngOnInit(): void {
    this.mapService.getPosition().then(
      (coordinates) => {
        this.userCoordinates = coordinates;
        this.stations$ = this.stationService.getNearestStations(
          coordinates.latitude,
          coordinates.longitude
        );
      },
      () => (this.locationDisabled = true)
    );
  }

  showCompanyAllStationsList(company: Company): void {
    this.company = company;
    this.showDialog = true;
  }

  hide(): void {
    this.company = null;
    this.showDialog = false;
  }
}
