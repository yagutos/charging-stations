import {
  Component,
  Input,
  EventEmitter,
  Output,
  ChangeDetectionStrategy
} from '@angular/core';
import { Company } from 'src/app/core/models/company.model';

@Component({
  selector: 'app-stations-list',
  templateUrl: './stations-list.component.html',
  styleUrls: ['./stations-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StationsListComponent {
  @Input()
  showDialog = false;

  @Input()
  record: Company;

  @Output()
  onHide = new EventEmitter<void>();

  hide(): void {
    this.onHide.emit();
  }
}
