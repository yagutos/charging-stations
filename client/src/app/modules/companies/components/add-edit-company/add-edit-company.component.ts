import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  OnInit,
  ChangeDetectionStrategy
} from '@angular/core';
import { Company } from 'src/app/core/models/company.model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-edit-company',
  templateUrl: './add-edit-company.component.html',
  styleUrls: ['./add-edit-company.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddEditCompanyComponent implements OnInit, OnChanges {
  private initialState: Company = {
    name: null
  };

  @Input()
  showDialog = false;

  @Input()
  parentId: string = null;

  @Input()
  record: Company;

  @Output()
  onSave = new EventEmitter<Company>();

  @Output()
  onCancel = new EventEmitter<void>();

  ngOnInit(): void {
    this.recordInit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.record) {
      this.recordInit();
    } else {
      this.record = { ...this.record };
    }
  }

  onSubmit(submittedForm: NgForm): void {
    if (submittedForm.invalid) {
      return;
    }

    const company: Company = this.record;
    if (this.parentId) company.parent_company = this.parentId;

    this.onSave.emit(company);
    this.recordInit();
  }

  cancel(): void {
    this.onCancel.emit();
    this.recordInit();
  }

  recordInit(): void {
    this.record = { ...this.initialState };
  }
}
