import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService, TreeNodeDragEvent } from 'primeng/api';
import { CompanyService } from 'src/app/core/http/company/company.service';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/store';
import { Subscription } from 'rxjs';
import { companyActions } from 'src/app/store/company/company.actions';
import { Company } from 'src/app/core/models/company.model';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.css'],
  providers: [ConfirmationService]
})
export class CompaniesComponent implements OnInit, OnDestroy {
  subscription: Subscription;

  rows: TreeNode[];
  cols: { field: string; header: string }[];

  loading: boolean;
  showDialog;
  showStationDialog: boolean;
  companyToEdit: Company;
  selectedNode;
  selectedParentId;

  constructor(
    private companyService: CompanyService,
    private store: Store<IAppState>,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.store.dispatch(companyActions.loadCompanies());

    this.cols = [{ field: 'name', header: 'Name' }];

    this.loading = true;
    this.resetDialog();
    this.loadNodes();
  }

  loadNodes(): void {
    this.loading = true;

    this.subscription = this.companyService
      .getCompaniesByParent(null)
      .subscribe((companies) => {
        this.loading = false;
        this.rows = [];

        companies.forEach((company) => {
          this.rows.push({
            data: company,
            leaf: !company.hasChildren
          });
        });
      });
  }

  onNodeExpand(event: TreeNodeDragEvent): void {
    const node = event.node;
    this.loading = true;

    this.companyService
      .getCompaniesByParent(node.data._id)
      .subscribe((companies) => {
        this.loading = false;

        node.children = companies.map((childCompany) => ({
          data: childCompany,
          leaf: !childCompany.hasChildren
        }));

        this.rows = [...this.rows];
      });
  }

  openAddCompanyDialog(parentId: string = null, node = null): void {
    this.selectedParentId = parentId;
    this.selectedNode = node;
    this.showDialog = true;
  }

  openStationsListDialog(company: Company, node: TreeNodeDragEvent): void {
    this.companyToEdit = company;
    this.showStationDialog = true;
  }

  openEditCompanyDialog(company: Company, node: TreeNodeDragEvent): void {
    this.companyToEdit = company;
    this.selectedNode = node;
    this.showDialog = true;
  }

  resetDialog(): void {
    this.companyToEdit = null;
    this.selectedNode = null;
    this.selectedParentId = null;
    this.showDialog = false;
    this.showStationDialog = false;
  }

  deleteCompany(company: Company, node: TreeNode): void {
    this.loading = true;
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete the company?',
      accept: () => {
        this.loading = false;
        this.companyService.deleteCompany(company._id).subscribe();
        this.deleteFromTree(company, node);
      }
    });
  }

  deleteFromTree(company: Company, node: TreeNode): void {
    if (node.parent) {
      node.parent.children = node.parent.children.filter(
        (cNode) => company._id !== cNode.data._id
      );
      this.rows = [...this.rows];

      node.parent.leaf = node.parent.children.length === 0;
    } else {
      this.rows = this.rows.filter((cNode) => company._id !== cNode.data._id);
    }
  }

  saveData(company: Company): void {
    if (!company._id) {
      this.addCompany(company, this.selectedNode);
    } else {
      this.editCompany(company, this.selectedNode);
    }

    this.resetDialog();
  }

  addCompany(company: Company, treeNode: TreeNodeDragEvent): void {
    this.loading = true;
    this.companyService.addCompany(company).subscribe((newCompany) => {
      this.loading = false;
      this.addCompanyToTree(newCompany, treeNode);
    });
  }

  addCompanyToTree(company: Company, treeNode: TreeNodeDragEvent): void {
    const newNode = {
      data: company,
      leaf: true
    };

    if (!treeNode) {
      this.rows = [...this.rows, newNode];
    } else {
      if (!treeNode.node.children) {
        treeNode.node.children = [];
        treeNode.node.leaf = false;
      }
      treeNode.node.children = [...treeNode.node.children, newNode];

      this.rows = [...this.rows];
    }
  }

  editCompany(company: Company, treeNode: TreeNodeDragEvent): void {
    this.loading = true;
    this.companyService.updateCompany(company._id, company).subscribe(() => {
      this.loading = false;
      treeNode.node.data = company;
    });
  }

  hideDialog(): void {
    this.resetDialog();
  }

  hideStationsDialog(): void {
    this.resetDialog();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
