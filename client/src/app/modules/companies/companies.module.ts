import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompaniesComponent } from './components/companies/companies.component';
import { CompaniesRoutingModule } from './companies-routing.module';
import { TreeTableModule } from 'primeng/treetable';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { AddEditCompanyComponent } from './components/add-edit-company/add-edit-company.component';
import { FormsModule } from '@angular/forms';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { StationsListComponent } from './components/stations-list/stations-list.component';
import { StationsModule } from '../stations/stations.module';

@NgModule({
  declarations: [
    CompaniesComponent,
    AddEditCompanyComponent,
    StationsListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    CompaniesRoutingModule,
    TreeTableModule,
    ButtonModule,
    DialogModule,
    ConfirmDialogModule,
    StationsModule
  ]
})
export class CompaniesModule {}
