import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StationsComponent } from './components/stations/stations.component';
import { StationsRoutingModule } from './stations-routing.module';
import { FormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AddEditStationComponent } from './components/add-edit-station/add-edit-station.component';
import { DialogModule } from 'primeng/dialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { SharedModule } from 'src/app/shared/shared.module';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [StationsComponent, AddEditStationComponent],
  exports: [StationsComponent, AddEditStationComponent],
  imports: [
    CommonModule,
    FormsModule,
    StationsRoutingModule,
    ButtonModule,
    TableModule,
    DialogModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    SharedModule,
    AgmCoreModule
  ]
})
export class StationsModule {}
