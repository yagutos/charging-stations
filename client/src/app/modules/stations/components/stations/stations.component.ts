import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectionStrategy
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Station } from 'src/app/core/models/station.model';
import { IAppState } from 'src/app/store';
import {
  getAllStations,
  areStationsLoading
} from 'src/app/store/station/station.selectors';
import { stationActions } from 'src/app/store/station/station.actions';
import { ConfirmationService } from 'primeng/api';
import { Update } from '@ngrx/entity';
import { Company } from 'src/app/core/models/company.model';
import { Coordinates } from 'src/app/core/models/coordinates.model';
import { MapService } from 'src/app/shared/services/map.service';

@Component({
  selector: 'app-stations',
  templateUrl: './stations.component.html',
  styleUrls: ['./stations.component.css'],
  providers: [ConfirmationService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StationsComponent implements OnInit, OnChanges {
  stations$: Observable<Station[]>;
  loading$: Observable<boolean>;

  stationToBeUpdated: Station;

  showDialog;
  stationToEdit: Station;

  @Input()
  company: Company = null;

  @Input()
  userCoordinates: Coordinates;

  constructor(
    private store: Store<IAppState>,
    private confirmationService: ConfirmationService,
    private mapService: MapService
  ) {}

  ngOnInit(): void {
    this.loading$ = this.store.select(areStationsLoading);
    this.stations$ = this.store.select(getAllStations);

    if (!this.company) {
      this.store.dispatch(stationActions.loadStations());
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.company?.currentValue) {
      this.store.dispatch(stationActions.setLoadingTrue());
      this.store.dispatch(
        stationActions.loadStationsByCompanyId({ id: this.company._id })
      );
    }
  }

  resetDialog(): void {
    this.stationToEdit = null;
    this.showDialog = false;
  }

  deleteStation(station: Station): void {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete the station?',
      accept: () => {
        this.store.dispatch(stationActions.deleteStation({ id: station._id }));
      }
    });
  }

  openAddStationDialog(): void {
    this.showDialog = true;
  }

  openEditStationDialog(station: Station): void {
    this.stationToBeUpdated = { ...station };
    this.showDialog = true;
  }

  saveData(station: Station): void {
    if (!station._id) {
      this.store.dispatch(stationActions.createStation({ station }));
    } else {
      const update: Update<Station> = {
        id: this.stationToBeUpdated._id,
        changes: station
      };
      this.store.dispatch(stationActions.updateStation({ update }));
    }

    this.resetDialog();
  }

  hideDialog(): void {
    this.resetDialog();
  }

  getCoordinates(station: Station): number {
    const secondCoord: Coordinates = {
      latitude: station.coordinates[1],
      longitude: station.coordinates[0]
    };
    return this.mapService.getDistance(this.userCoordinates, secondCoord);
  }
}
