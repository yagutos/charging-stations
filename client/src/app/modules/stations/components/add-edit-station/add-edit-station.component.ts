import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  OnInit,
  ChangeDetectionStrategy
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Station } from 'src/app/core/models/station.model';
import { CompanyService } from 'src/app/core/http/company/company.service';
import { Company } from 'src/app/core/models/company.model';
import { Coordinates } from 'src/app/core/models/coordinates.model';
import { MapService } from 'src/app/shared/services/map.service';

interface AutoCompleteEvent {
  event: unknown;
  query: string;
}

interface MapPoint {
  lat: number;
  lng: number;
  label: string;
}

interface MapEvent {
  coords: MapPoint;
}

@Component({
  selector: 'app-add-edit-station',
  templateUrl: './add-edit-station.component.html',
  styleUrls: ['./add-edit-station.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddEditStationComponent implements OnInit, OnChanges {
  private initialState: Station = {
    name: null,
    coordinates: [0, 0],
    company: null
  };

  public latitude: number;
  public longitude: number;

  locationDisabled: boolean;
  userCoordinates: Coordinates;
  points: MapPoint[];

  results: Company[];

  @Input()
  showDialog = false;

  @Input()
  parentId: string = null;

  @Input()
  record: Station;

  @Output()
  onSave = new EventEmitter<Station>();

  @Output()
  onCancel = new EventEmitter<void>();

  constructor(
    private companyService: CompanyService,
    private mapService: MapService
  ) {}

  ngOnInit(): void {
    this.recordInit();
  }

  getMyLocation(): MapPoint {
    return {
      lat: this.userCoordinates?.latitude,
      lng: this.userCoordinates?.longitude,
      label: 'My Location'
    };
  }

  loadUserCoordinates(): void {
    this.points = [this.getMyLocation()];
    if (this.locationDisabled !== undefined || this.userCoordinates) {
      return;
    }
    this.mapService.getPosition().then(
      (coordinates) => (this.userCoordinates = coordinates),
      () => (this.locationDisabled = true)
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.loadUserCoordinates();

    if (!changes.record?.currentValue) {
      this.recordInit();
    } else {
      this.record = { ...this.record };
      this.longitude = this.record.coordinates[0];
      this.latitude = this.record.coordinates[1];
    }
  }

  onSubmit(submittedForm: NgForm): void {
    if (submittedForm.invalid) {
      return;
    }

    const station: Station = this.record;
    station.coordinates = [this.longitude, this.latitude];

    this.onSave.emit(station);
    this.recordInit();
  }

  cancel(): void {
    this.onCancel.emit();
    this.recordInit();
  }

  recordInit(): void {
    this.record = { ...this.initialState };
    this.latitude = null;
    this.longitude = null;
  }

  search(event: AutoCompleteEvent): void {
    this.companyService.getCompaniesByName(event.query).subscribe((data) => {
      this.results = data;
    });
  }

  mapClicked($event: MapEvent): void {
    this.points = [
      this.getMyLocation(),
      {
        lat: $event.coords.lat,
        lng: $event.coords.lng,
        label: 'New Station'
      }
    ];
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
  }
}
