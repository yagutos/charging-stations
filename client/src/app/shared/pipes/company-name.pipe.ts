import { Pipe, PipeTransform } from '@angular/core';
import { Company } from 'src/app/core/models/company.model';

@Pipe({
  name: 'companyName',
  pure: true
})
export class CompanyNamePipe implements PipeTransform {
  transform(company: Company | string): string {
    return typeof company === 'string' ? company : company?.name;
  }
}
