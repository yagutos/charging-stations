import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Subscription } from 'rxjs';
import { NotificationsService } from './notifications.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css'],
  providers: [MessageService]
})
export class NotificationsComponent implements OnInit {
  subscription: Subscription;

  constructor(
    private notificationsService: NotificationsService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.subscribeToNotifications();
  }

  subscribeToNotifications(): void {
    this.subscription = this.notificationsService.notificationChange.subscribe(
      (notification) => {
        this.messageService.add(notification);
      }
    );
  }

  clear(): void {
    this.messageService.clear();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
