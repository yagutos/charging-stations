import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ToastModule } from 'primeng/toast';
import { NotificationsService } from './components/notifications/notifications.service';
import { CompanyNamePipe } from './pipes/company-name.pipe';
import { MapService } from './services/map.service';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [NotificationsComponent, CompanyNamePipe, NotFoundComponent],
  imports: [CommonModule, ToastModule, RouterModule],
  exports: [NotificationsComponent, CompanyNamePipe, NotFoundComponent],
  providers: [NotificationsService, MapService]
})
export class SharedModule {}
