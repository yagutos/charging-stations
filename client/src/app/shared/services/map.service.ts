import { Injectable } from '@angular/core';
import { Coordinates } from 'src/app/core/models/coordinates.model';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  getPosition(): Promise<Coordinates> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (resp) => {
          resolve({
            longitude: resp.coords.longitude,
            latitude: resp.coords.latitude
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  getDistance(coordinate1: Coordinates, coordinate2: Coordinates): number {
    const R = 6371.071; // Radius of the Earth in miles
    const rlat1 = coordinate1.latitude * (Math.PI / 180); // Convert degrees to radians
    const rlat2 = coordinate2.latitude * (Math.PI / 180); // Convert degrees to radians
    const difflat = rlat2 - rlat1; // Radian difference (latitudes)
    const difflon =
      (coordinate2.longitude - coordinate1.longitude) * (Math.PI / 180); // Radian difference (longitudes)

    const d =
      2 *
      R *
      Math.asin(
        Math.sqrt(
          Math.sin(difflat / 2) * Math.sin(difflat / 2) +
            Math.cos(rlat1) *
              Math.cos(rlat2) *
              Math.sin(difflon / 2) *
              Math.sin(difflon / 2)
        )
      );
    return d;
  }
}
