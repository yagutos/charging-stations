import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './modules/home/home.module';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { appReducers } from './store';
import { environment } from 'src/environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StationEffects } from './store/station/station.effects';
import { CompanyEffects } from './store/company/company.effects';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    CoreModule,
    SharedModule,
    HomeModule,
    StoreModule.forRoot(appReducers),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([StationEffects, CompanyEffects]),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC0wCCyQhPywWBBBucadI0f2s7oKpSLY50'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
