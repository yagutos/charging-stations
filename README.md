# Charging Stations

Technologies:

- Back: Node.JS
- Front: Angular
- Database: Cloud MongoDB

#### Demo

[https://immense-badlands-35111.herokuapp.com/](https://immense-badlands-35111.herokuapp.com/)

#### The database schema:

    1. Station (id, name, latitude, longitude, company)
    2. Company (id, parent_company, name, ancestors)

### Installation

Charging Stations requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ git clone git@bitbucket.org:yagutos/charging-stations.git .
$ npm install
$ npm run clientinstall
```

Run in local envirement...

```sh
$ Both servers: -> npm run dev-all
$ Only Angular -> npm run client
$ Only NodeJS -> npm run dev
```

Verify the deployment by navigating to your server address in your preferred browser.

```sh
Angular: 127.0.0.1:4000
NodeJS:  127.0.0.1:3000
```

## License

MIT
